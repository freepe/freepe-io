module.exports = function(mctx,cbfail,cbok){
///////////
var api = mctx.api;
var db = mctx.db;
var Wallets = mctx.Wallets;
api.getMyWallets = function(obj,cbfail,cbok){
	if(!Wallets){
		console.log('Wallets class not available');
		return cbfail('Wallets not available yet');
	}
	var query, params;
	if(obj.uid){
		query = 'select address, name, description from Wallets where owner = :uid';
		params = {
			uid: obj.uid
		}
	}else{
		cbfail('undefined user id');
		return;
	}
	db.query(query,{
		params: params
	})
	.then(function(data){
		cbok(data);
	});
}
api.setMyWallets = function(obj,cbfail,cbok){
	if(!Wallets){
		console.log('Wallets class not available');
		return cbfail('Wallets not available yet');
	}
	api.getMyWallets({
		uid: obj.uid
	}, fail => {
		cbfail(fail);
	}, ok => {
		if(!ok.length){
			// var query, params;
			// if(obj.uid){
			// 	query = 'select address, name, description from Wallets where owner = :uid';
			// 	params = {
			// 		uid: obj.uid
			// 	}
			// }else{
			// 	cbfail('undefined user id');
			// 	return;
			// }
			// db.query(query,{
			// 	params: params
			// })
			// .then(function(data){
			// 	cbok(data);
			// });
		}else{
			return cbfail('Wallets for user already created');
		}
	});
}
cbok();
///////////
}