"use strict";

var schema = [
	{
		name: 'owner',
		type: 'String',
		mandatory: true,
	},
	{
		name: 'address',
		type: 'String',
		mandatory: true,
	},
	{
		name: 'name',
		type: 'String',
		max: 40
	},
	{
		name: 'description',
		type: 'String',
		max: 500
	},
	{
		name: 'time',
		type: 'Long',
		mandatory: true
	},
];

var indexes = [
	{
		name: 'Wallets.address',
		type:'unique'
	},
];


module.exports = {
	name: 'Wallets',
	schema:schema,
	indexes: indexes
};