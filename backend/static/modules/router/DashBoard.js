DC.module('router/DashBoard', function (mctx) {
    //////////
    function f_dc_make(obj) {
        return DC.make(obj);
    }
    function f_dc_temp(obj) {
        return DC.temp(obj);
    }
    var dc_module = DC.temp(function () {
        return {
            state: {
                class: 'module-room'
            },
            init() {
                return this;
            }
        }
    });

    var content = f_dc_temp({
        state: {
            class: 'content'
        }
    }).insertIn(dc_module);
    ///////////////////////////////////////////////////////////////////////////////
    // block character
    var character = f_dc_temp({
        state: {
            class: 'character'
        }
    }).insertIn(content);

    var account = f_dc_temp({
        state: {
            class: 'characterDiv',
            html: `<span>Ваши Счета</span>`
        }
    }).insertIn(character);

    var divSh = f_dc_temp({
        state: {
            class: 'sh',
            html: ''
        }
    }).insertIn(account);

    var fpn = f_dc_temp({
        eltype: 'span',
        state: {
            class: 'fpn',
            html: 'FPN'
        }, events: {
            click() {
                fpn.addClass('active_VAL');
                fpm.removeClass('active_VAL');
            }
        }
    }).insertIn(divSh)
    fpn.addClass('active_VAL');

    var fpm = f_dc_temp({
        eltype: 'span',
        state: {
            class: 'fpm',
            html: 'BTC'
        },
        events: {
            click() {
                fpm.addClass('active_VAL');
                fpn.removeClass('active_VAL');
            }
        }
    }).insertIn(divSh)

    var arrow = f_dc_temp({
        state: {
            class: 'arrowBig',
            html: '<img src="images/arrowBig.png" style="vertical-align: top;">'
        }, events: {
            click() {
                if (slideRef.el.style.display == 'block' || slideSkils.el.style.display == 'block') {
                    slideSkils.el.hide();
                    slideRef.el.hide();
                }
                if (slideAccount.el.style.display !== 'none')
                    slideAccount.el.hide();
                else
                    slideAccount.el.show();
            }
        },
        attrs: {
            style: 'cursor:pointer'
        }
    }).insertIn(account);

    var slideAccount = f_dc_temp({
        eltype: 'ul',
        state: {
            html: `
                <li>Кошелек: <i>Личный</i> <img src="images/copy.png" 
                                            style="margin-left: 10px;vertical-align: sub;">
                </li>
				<li>Баланс: <i>10 000 FPN</i></li>
        `,
            class: 'slideAccount'
        }
    }).insertIn(account);
    slideAccount.el.hide();

    var refer = f_dc_temp({
        state: {
            class: 'characterDiv ref',
            html: `<span>Ваши Рефералы</span>
                
                `
        }
    }).insertIn(character);

    var refCount = f_dc_temp({
        eltype: 'span',
        state: {
            class: 'refCount',

        }
    }).insertIn(refer);

    var refLabel = f_dc_temp({
        eltype: 'label',
        state: {
            text: 150
        }
    }).insertIn(refCount);

    var chel = f_dc_temp({
        eltype: 'text',
        state: {
            text: ' чел.'
        }
    }).insertIn(refLabel)


    var arrowRef = f_dc_temp({
        state: {
            class: 'arrowBigRef',
            html: '<img src="images/arrowBig.png" style="vertical-align: top;">'
        },
        events: {
            click() {
                if (slideAccount.el.style.display == 'block' || slideSkils.el.style.display == 'block') {
                    slideSkils.el.hide();
                    slideAccount.el.hide();
                }
                if (slideRef.el.style.display !== 'none')
                    slideRef.el.hide();
                else
                    slideRef.el.show();
            }
        },
        attrs: {
            style: 'cursor: pointer'
        }
    }).insertIn(refer);

    var slideRef = f_dc_temp({
        eltype: 'ul',
        state: {
            class: 'slideRef',
            html: `
            <li>1 Виктор <em>   15.02.2016   </em>   <span>600 </span>FPN</li>
                            <li>2 Виктор <em>15.02.2016</em>   <span>600 </span>FPN</li>
        `
        }
    }).insertIn(refer);
    slideRef.el.hide();

    var scills = f_dc_temp({
        state: {
            class: 'characterDiv skils',
            html: `<span>Ваш Уровень</span>`
        },
        events: {
        }
    }).insertIn(character);

    var skillsNumber = f_dc_temp({
        eltype: 'span',
        state: {
            text: '0.5',
            class: 'skillsNumber'
        }
    }).insertIn(scills);
    var arrowScills = f_dc_temp({
        state: {
            class: 'arrowBigScills',
            html: '<img src="images/arrowBig.png" style="vertical-align: top;">'
        },
        events: {
            click() {
                if (slideAccount.el.style.display == 'block' || slideRef.el.style.display == 'block') {
                    slideRef.el.hide();
                    slideAccount.el.hide();
                }
                if (slideSkils.el.style.display !== 'none')
                    slideSkils.el.hide();
                else
                    slideSkils.el.show();
            }
        },
        attrs: {
            style: 'cursor: pointer'
        }
    }).insertIn(scills);

    var slideSkils = f_dc_temp({
        eltype: 'ul',
        state: {
            html: `<li>2 реферала = 40 FPT</li>
							<li>10 FPN = 10 FPT</li>	
							<li>50 FPT до уровня 1</li>`,
            class: 'slideSkils'
        }
    }).insertIn(scills);
    slideSkils.el.hide();

    var rating = f_dc_temp({
        state: {
            class: 'characterDiv raiting',
            html: `<span>Ваш Рейтинг</span>`
        }
    }).insertIn(character);

    var ratingCount = f_dc_temp({
        eltype: 'span',
        state: {
            text: '0.968',
            class: 'raitingsNumber'
        }
    }).insertIn(rating);

    ///////////////////////////////////////////////////////////////////////////////////
    // block skillsNumber

    var pageSkills = f_dc_temp({
        state: {
            class: 'skillsDiv'
        }
    }).insertIn(content);

    var level1_block = f_dc_temp({
        state: {
             class: 'level_block'
        }
    }).insertIn(pageSkills);
    var level2_block = f_dc_temp({
        state: {
            class: 'level_block'
        }
    }).insertIn(pageSkills);
    var level3_block = f_dc_temp({
        state: {
             class: 'level_block'
        }
    }).insertIn(pageSkills);
    var level4_block = f_dc_temp({
        state: {
            class: 'level_block'
        }
    }).insertIn(pageSkills);

    var level1 = f_dc_temp({

        state: {
            html: '<h1>УРОВЕНЬ 1</h1>',
            class: 'level1'
        },
        attrs: {
            src: 'images/skills1.png'
        }
    }).insertIn(level1_block);

    var level2 = f_dc_temp({
        state: {
            html: '<h1>УРОВЕНЬ 2</h1>',
            class: 'level2'
        },
        attrs: {
            src: 'images/skills2.png'
        }
    }).insertIn(level2_block);

    var level3 = f_dc_temp({
        state: {
            html: '<h1>УРОВЕНЬ 3</h1>',
            class: 'level3'
        },
        attrs: {
            src: 'images/skills3.png'
        },
        events: {
            click() {
                if (levelUL.el.style.display !== 'none')
                    levelUL.el.hide();
                else
                    levelUL.el.show();
            }
        }
    }).insertIn(level3_block);

    var level4 = f_dc_temp({
        state: {
            html: '<h1>УРОВЕНЬ 4</h1>',
            class: 'level4'
        },
        attrs: {
            src: 'images/skills4.png'
        }
    }).insertIn(level4_block);

    var levelUL = f_dc_temp({
        eltype: 'ul',
        state: {
            html: '<li>'
            + '<span>Задача</span><br>'
            + 'Пригласите 10 друзей'
            + '</li>'
            + '<li>'
            + '<span>Награда</span><br>'
            + '	15% с инвестиций'
            + '</li>',
            class: 'slideSkills'
        }
    }).insertIn(level3_block);
    levelUL.el.hide();

    /////////////////////////////////////////////////
    //block main

    var main = f_dc_temp({
        state: {
            class: 'main'
        }
    }).insertIn(content);

    var categories = f_dc_temp({
        state: {
            html: `<ul>
                    <li class="personal li"><a href="#" onmouseover="">ЛИЧНЫЕ</a></li>
                    <li class="work li"><a href="">СЛУЖЕБНЫЕ</a></li>
                    <li class="group li"><a href="">ГРУППОВЫЕ</a></li>
                    <li class="other"><a href=""></a></li>
                    <div class="arrow sol"></div>
                    <li class="icon3 icon li" style="display:none;"><img src="images/one.png"></li>
                    <li class="icon2 icon li"><img src="images/three.png"></li>
                    <li class="icon1 icon li"><img src="images/two.png"></li>
            </ul>`,
            class: 'categories'
        }
    }).insertIn(main);



    //////////////////////////////////////////////////////////////////////////////
    // block people left nav bar

    var blockPeople = f_dc_temp({
        state: {
            class: 'blockPeople',
            html: `
                <li class="li one">
                    <span>Дмитрий Васильев</span><em></em><br>
							Lore ipsum wbubw
                </li>
                 <li class="li">
                    <span>Дмитрий Васильев</span><em></em><br>
							Lore ipsum wbubw
                </li>
                 <li class="li">
                    <span>Дмитрий Васильев</span><em></em><br>
							Lore ipsum wbubw
                </li>
                 <li class="li">
                    <span>Дмитрий Васильев</span><em></em><br>
							Lore ipsum wbubw
                </li>
                <li class="li">
                    <span>Дмитрий Васильев</span><em></em><br>
							Lore ipsum wbubw
                </li>

            `
        }
    }).insertIn(main);


    //////////////////////////////////////////////////////////////////////////////////
    // block messages

    var messages = f_dc_temp({
        state: {
            class: 'message',
            html: ''
        }
    }).insertIn(main);

    var blockM = f_dc_temp({
        state: {
            class: 'block-message',
            html: `
                <img src="images/messageLeftImage.png" class="messageLeftImage">
                <div class="messageLeft"><p>Lorem iefwwefwepsum dolor sit</p></div><br>
                <div class="messageRighttoo"><p>Lorem ipsum dolor sit </p></div>
                <img src="images/messageRightImage.png" class="messageRightImagetoo"><br>
                <img class="messageLeftImagetoo" src="images/messageLeftImage.png">
                <div class="messageLefttoo"><p>Lorem ipsum dolor sit </p></div><br>
                <div class="messageRight"><p>Lorem ipsum dolor sit </p></div>
                <img src="images/messageRightImage.png" class="messageRightImage">
            `
        }
    }).insertIn(messages);

    var inputField = f_dc_temp({
        state: {
            class: 'inputField'
        }
    }).insertIn(messages);

    var plus = f_dc_temp({
        eltype: 'em',
        state: {
            class: 'plus',
            html: '+'
        }
    }).insertIn(inputField);

    var plus = f_dc_temp({
        eltype: 'img',
        state: {
            class: 'smile',
            html: '+'
        },
        attrs: {
            src: 'images/smile.png'
        }
    }).insertIn(inputField);

    var inputText = f_dc_temp({
        eltype: 'input',
        state: {
            class: 'text',
            html: ''
        },
        attrs: {
            src: 'images/smile.png',
            type: 'text'
        }
    }).insertIn(inputField);


    var inputSubmit = f_dc_temp({
        eltype: 'input',
        state: {
            class: 'submit',
            text: 'ОТПРАВИТЬ'
        },
        attrs: {
            src: 'images/smile.png',
            type: 'submit'
        },
        events: {
            click() {
                if (inputText.el.val() !== '') {
                    console.log(inputText.el.val());
                    inputText.el.val('');
                }

            }
        }
    }).insertIn(inputField);

    ///////////////////////////////////////////////////////////////////////////////
    //block refer and news

    var blockFn = f_dc_temp({
        state: {
            class: 'blockrefN'
        }
    }).insertIn(main);

    var referen = f_dc_temp({
        state: {
            class: 'referen',
            html: `<h3>РАССКАЖИТЕ ДРУЗЬЯМ: </h3>`
        }
    }).insertIn(blockFn);

    var referenInput = f_dc_temp({
        eltype: 'input',
        state: {
            class: 'refText'
        },
        attrs: {
            type: 'text',
            value: 'loading...',
            id: 'refer'
        }
    }).insertIn(referen);

    // var copy = f_dc_temp({
    //     eltype: 'img',
    //     state: {
    //         class: 'copy1'
    //     },
    //     attrs: {
    //         src: 'images/copy.png'
    //     }
    // }).insertIn(referen);

    var social = f_dc_temp({
        state: {
            html: `<p data-clipboard-target="#refer" class="btn-clipboard"><img src="images/copy.png" class="social_copy" > <a>Копировать</a></p>`,
            class: 'social'
        }
    }).insertIn(referen);
    /* social button */
    /*var social_copy = f_dc_temp({
        eltype: 'p',
        state: {
            html: '<img src="images/copy.png" class="social_copy" > <a>Копировать</a>'
        },
        events: {
            click(){

            }
        },
        attrs: {
            class: 'btn-clipboard',
            dataClipboardTarget: '#refer'
        }
    }).insertIn(social);*/
    new Clipboard('.btn-clipboard');

    var facebook = f_dc_temp({
        eltype: 'a',
        state: {
            html: '<img src="images/facebook.png">'
        },
        events: {
            click() {
                event.preventDefault();
                return share.me(this);
            }
        },
        attrs: {
            href: 'http://www.facebook.com/sharer/sharer.php?s=100&p%5Btitle%5D=' + 'Реферальная ссылка http://freepe.io/ref/' + mctx.User.ref_link + '&p%5Burl%5D=URL&p%5Bimages%5D%5B0%5D=IMG_PATH',
            target: '_blank'
        }
    }).insertIn(social);
    var twitter = f_dc_temp({
        eltype: 'a',
        state: {
            html: '<img src="images/twitter.png">'
        },
        events: {
            click() {
                event.preventDefault();
                return share.me(this);
            }
        },
        attrs: {
            href: 'https://twitter.com/intent/tweet?original_referer=http%3A%2F%2Ffiddle.jshell.net%2F_display%2F&text=' + 'Реферальная ссылка http://freepe.io/ref/' + mctx.User.ref_link + '&url=URL',
            target: '_blank'
        }
    }).insertIn(social);
    var vk = f_dc_temp({
        eltype: 'a',
        state: {
            html: '<img src="images/vk.png">'
        },
        events: {
            click(event) {
                console.log(event);
                event.preventDefault();
                return share.me(this);
            }
        },
        attrs: {
            href: 'http://vk.com/share.php?url=URL&title=' + 'Реферальная ссылка http://freepe.io/ref/' + mctx.User.ref_link + '&image=IMG_PATH&noparse=true',
            target: '_blank'
        }
    }).insertIn(social);
    var gmail = f_dc_temp({
        eltype: 'a',
        state: {
            html: '<img src="images/google-plus.png">'
        },
        events: {
            click() {
                event.preventDefault();
                return share.me(this);
            }
        },
        attrs: {
            href: 'http://connect.mail.ru/share?url=URL&title=' + 'Реферальная ссылка http://freepe.io/ref/' + mctx.User.ref_link + '&imageurl=IMG_PATH',
            target: '_blank'
        }
    }).insertIn(social);

    var share = {
        vkontakte: function (purl, ptitle, pimg, text) {
            url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            console.log('vkontacte');
            share.popup(url);
        },
        facebook: function (purl, ptitle, pimg, text) {
            url = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]=' + encodeURIComponent(ptitle);
            url += '&p[summary]=' + encodeURIComponent(text);
            url += '&p[url]=' + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            share.popup(url);
        },
        twitter: function (purl, ptitle) {
            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            share.popup(url);
        },
        mailru: function (purl, ptitle, pimg, text) {
            url = 'http://connect.mail.ru/share?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl=' + encodeURIComponent(pimg);
            share.popup(url)
        },

        me: function (el) {
            console.log(el.href);
            share.popup(el.href);
            return false;
        },

        popup: function (url) {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }

    };
    /* social button end */

    var news = f_dc_temp({
        state: {
            class: 'news',
            html: '<h3>НОВОСТИ</h3>'
        }
    }).insertIn(blockFn);

    var newsSpisok = f_dc_temp({
        state: {
            class: 'ul',
            html: `
						<ul>
							<li> <span>Пользователь купил 100 FPT</span></li>
							<li> <span>Новый Пользователь "12345" <br> присоеднился</span></li>
							<li><span>Пользователь 134 купил 20 FPT</span></li>
							<li><span>Пользователь 1234 купил 10 FPT</span></li>
							
						</ul>
					`
        }
    }).insertIn(news);
    var news_bottom = f_dc_temp({
        eltype: 'h3',
        state: {
            class: 'news_bottom',
            html: '<img src="images/news_arrow.png">'
        },
        attrs: {
            style: 'cursor:pointer'
        }
    }).insertIn(news);




    var api_module = {
        updateLink(val) {
            // referenInput.change({

            // })
            referenInput.el.value = 'http://freepe.io/ref/' + val;
        },
        updateLevel(val) {
            skillsNumber.change({
                text: val
            })
        },
        updateReferals(val) {
            //
            refLabel.change({
                text: val
            })
        },
        updateAccounts(val) {
            //
            var str = '';
            str += '<li>';
            slideAccount.change({
                html: ''
            })
        }

    }

    if (mctx.User) api_module.updateLink(mctx.User.ref_link);
    api_module.updateLevel(mctx.User.getLevel());
    api_module.updateReferals(4);
    //api_module.updateAccounts({});

    return {
        //////
        dc: dc_module,
        api: api_module

    }
});